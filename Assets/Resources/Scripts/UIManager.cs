using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [NonSerialized] public static UIManager i;
    [SerializeField] public TextMeshProUGUI TMPMonedas;
    [SerializeField] public TextMeshProUGUI TMPGuardado;
    [SerializeField] public TextMeshProUGUI TMPJ1;

    private void Awake()
    {
        CrearSingleton();
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
}
