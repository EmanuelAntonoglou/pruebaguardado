using UnityEngine;

[System.Serializable]
public class PlayerStats
{
    [HideInInspector] public string Id = "";
    public float Vidas = 3;
    public Vector3 Position = Vector3.zero;
    public Quaternion Rotation = Quaternion.identity;
    public Vector3 Scale = Vector3.zero;
}