using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Collections;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public GameStats Juego;
    public List<PlayerStats> Jugador;

    public GameData() 
    {
        Juego = new GameStats();
        Jugador = new List<PlayerStats>();
    }
}
