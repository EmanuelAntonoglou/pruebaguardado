using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Identifier : MonoBehaviour
{
    public string Id = "";

    [ContextMenu("Generate Guid for Id")]
    private void GenerateGuid()
    {
        Id = System.Guid.NewGuid().ToString();
    }

    private void Awake()
    {
        if (Id == "")
        {
            Id = gameObject.name;
        }
    }
}
