using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting.Antlr3.Runtime.Collections;
using UnityEngine;

public class DataPersistenceManager : MonoBehaviour
{
    [Header("File Storage Configuration")]
    [SerializeField] private string extension;

    public static DataPersistenceManager i { get; private set; }
    public bool useEncryption = true;
    public GameData gameData;
    public List<IDataPersistence> objetosParaGuardar;
    private FileDataHandler dataHandler;

    private void Awake()
    {
        CrearSingleton();
        objetosParaGuardar = new List<IDataPersistence>();
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        dataHandler = new FileDataHandler(Application.persistentDataPath, ("Save" + "1" + extension), useEncryption);
        LoadGame();
    }

    public void LoadGame()
    {
        gameData = dataHandler.Load();
        CargarData();
    }

    public void SaveGame() 
    {
        gameData = new GameData();
        GuardarData();
        dataHandler.Save(gameData);
    }
    
    private void GuardarData()
    {
        for (int i = 0; i < objetosParaGuardar.Count; i++)
        {
            objetosParaGuardar[i].SaveData(ref gameData);
        }
    }

    private void CargarData()
    {
        for (int i = 0; i < objetosParaGuardar.Count; i++)
        {
            objetosParaGuardar[i].LoadData(gameData);
        }
    }
}
