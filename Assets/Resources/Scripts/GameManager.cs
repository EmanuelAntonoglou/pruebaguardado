using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class GameManager : MonoBehaviour, IDataPersistence
{
    [NonSerialized] public static GameManager i;
    public GameStats gameStats;
    public ControlJugador J1;
    public ControlJugador J2;

    private void Awake()
    {
        CrearSingleton();
        DataPersistenceManager.i.objetosParaGuardar.Add(this);
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            gameStats.Monedas++;
            UIManager.i.TMPMonedas.text = "Monedas: " + gameStats.Monedas;
        }

        if (Input.GetKey(KeyCode.A))
        {
            J1.transform.Translate(300 * Time.deltaTime * Vector3.left);
        }
        if (Input.GetKey(KeyCode.D))
        {
            J1.transform.Translate(300 * Time.deltaTime * Vector3.right);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            J2.transform.Translate(300 * Time.deltaTime * Vector3.left);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            J2.transform.Translate(300 * Time.deltaTime * Vector3.right);
        }
    }

    public void LoadData(GameData data)
    {
        gameStats = data.Juego;
        UIManager.i.TMPMonedas.text = "Monedas: " + data.Juego.Monedas.ToString();
    }

    public void SaveData(ref GameData data)
    {
        data.Juego = gameStats;
    }
}
