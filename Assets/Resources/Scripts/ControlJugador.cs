using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using Unity.VisualScripting.Antlr3.Runtime.Collections;
using UnityEngine;

public class ControlJugador : MonoBehaviour, IDataPersistence
{
    public PlayerStats Stats;

    private void Awake()
    {
        DataPersistenceManager.i.objetosParaGuardar.Add(this);
    }

    public void LoadData(GameData data)
    {
        string Id = GetComponent<Identifier>().Id;
        PlayerStats objetoEnGuardado = data.Jugador.FirstOrDefault(stats => stats.Id == Id); //Encontrar el Id del objeto en el save

        if (objetoEnGuardado != null)
        {
            transform.position = objetoEnGuardado.Position;
            transform.rotation = objetoEnGuardado.Rotation;
            transform.localScale = objetoEnGuardado.Scale;
        }
    }

    public void SaveData(ref GameData data)
    {
        Stats.Id = GetComponent<Identifier>().Id;
        Stats.Position = transform.position;
        Stats.Rotation = transform.rotation;
        Stats.Scale = transform.localScale;
        
        data.Jugador.Add(Stats);
    }
}
