using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuGuardar : MonoBehaviour
{
    public void OnNewGameClicked()
    {
        DataPersistenceManager.i.SaveGame();
    }

    public void OnSaveGameClicked()
    {
        DataPersistenceManager.i.SaveGame();
    }

    public void OnLoadGameClicked()
    {
        DataPersistenceManager.i.LoadGame();
    }
}
